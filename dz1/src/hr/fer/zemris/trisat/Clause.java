package hr.fer.zemris.trisat;


public class Clause {
	
	private int[] indexes;
	
	
	public Clause(int[] indexes) {
		if (indexes.length != 3) {
			throw new IllegalArgumentException("Not 3 variables in clause");
		}
		this.indexes = indexes.clone();
		
	}
	
	// vraća broj literala koji čine klauzulu
	public int getSize() {
		return indexes.length;
	}
	
	// vraća indeks varijable koja je index-ti član ove klauzule
	public int getLiteral(int index) {
		if(index < 0 || index >indexes.length) {
			throw new IllegalArgumentException("Index out of range");
		}
		return indexes[index];
	}
	
	// vraća true ako predana dodjela zadovoljava ovu klauzulu
	public boolean isSatisfied(BitVector assignment) {
		for (int i : indexes) {
			boolean value = assignment.get(Math.abs(i) - 1);
			boolean expectedValue;
			if (i > 0) {
				expectedValue = true;
			}
			else {
				expectedValue = false;
			}
			if (expectedValue == value) {
				return true;
			}
			
		}
		return false;
	}
	
	@Override
	public String toString() {
		String returnString = "";
		for (int i = 0; i < indexes.length; i++) {
			if (this.getLiteral(i) >= 0) {
				returnString += "X" + Integer.toString(i);
			}
			else {
				returnString += "\u0021" + "X" + Integer.toString(i);
			}
		}
		
		return returnString;
	}
}
