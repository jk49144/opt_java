package hr.fer.zemris.trisat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
//RandomWalkSAT algoritam
public class Algoritam5 {
	
	private final int MAX_NUMBER_OF_ITERATIONS = 100000;
	private int  maxTries = 10;
	private double p = 0.5;
	
	public void solve(SATFormula formula) {
		for (int x = 0; x < maxTries; x++) {
			BitVector initial = new BitVector(new Random(), formula.getNumberOfVariables());
			SolutionCandidate solutionCandidate = new SolutionCandidate(initial, 0.0);
			int i = 0;
			SATFormulaStats stats = new SATFormulaStats(formula);
		
			while(i < MAX_NUMBER_OF_ITERATIONS) {
			
				stats.setAssignment(solutionCandidate.getBitVector(), true);
				solutionCandidate.setFitness(getFitness(stats));
				if(formula.getNumberOfClauses() == stats.getNumberOfSatisfied()) {
					System.out.println("Rjesenje je: " + solutionCandidate.getBitVector().toString() + 
							" nadeno" + i +"-tom koraku " + x + "te iteracije");
					return;
				}
				
				
				SolutionCandidate newSolution = changeCurrentSolution(solutionCandidate, stats, formula);
				if (newSolution.getFitness() < solutionCandidate.getFitness()) {
					System.out.println("Zapeli smo u lokalnom maksimumu koji iznos" + solutionCandidate.getBitVector().toString()+ "nakon kolkko iteracija" 
							+ i + "fitnes iznosi" +newSolution.getFitness());
					break;
				}
				solutionCandidate = newSolution;
				i++;
			}
			//System.out.println("Dosli do maksimalnog broja iteracija zadnji broj koji smo imali je:  " + solutionCandidate.getBitVector().toString()
					//+ "ciji je fitness" + solutionCandidate.getFitness());
		}
	}
	
	
	private double getFitness(SATFormulaStats stats) {
		return stats.getNumberOfSatisfied();
	}
	
	

	private SolutionCandidate changeCurrentSolution(SolutionCandidate solutionCandidate, SATFormulaStats stats, SATFormula formula) {
		boolean[] values = new boolean[solutionCandidate.getBitVector().getSize()];
		for(int i = 0; i < solutionCandidate.getBitVector().getSize(); i++) {
			values[i] =solutionCandidate.getBitVector().get(i);
		}
		
		
		Random random = new Random();
		List<Clause> clauses = formula.nezadovoljeneKlauzule(solutionCandidate.getBitVector());
		int kojuKlauzulu = random.nextInt(clauses.size());
		Clause c = clauses.get(kojuKlauzulu);
		//krivo shvatio opis algorima ..moram na faks necu stic popravit do predaje
		for (int i = 0; i < c.getSize(); i++) {
			int x = Math.abs(c.getLiteral(i)) - 1;
			Double d = 100 * p;
			int l= d.intValue();
			if (random.nextInt(100) < l) {
				values[x] = !values[x];
			}
		}
		BitVector bitVector = new BitVector(values);
		stats.setAssignment(bitVector, false);

		return new SolutionCandidate(bitVector, getFitness(stats));
	}
	
}
