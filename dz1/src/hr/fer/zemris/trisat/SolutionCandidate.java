package hr.fer.zemris.trisat;

public class SolutionCandidate implements Comparable<SolutionCandidate>{
    private BitVector bVector;
    private double fitness;

    public SolutionCandidate(BitVector bVector, double fitness) {
    	super();
        this.bVector = bVector;
        this.fitness = fitness;
    }

    public BitVector getBitVector() {
        return bVector;
    }

    public void setBitVector(BitVector bVector) {
        this.bVector = bVector;
    }

    public double getFitness() {
        return fitness;
    }

    public void setFitness(double fitness) {
        this.fitness = fitness;
    }

	@Override
	public int compareTo(SolutionCandidate arg0) {
		// TODO Auto-generated method stub
		
		return Double.compare(fitness, arg0.fitness);
	}




}
