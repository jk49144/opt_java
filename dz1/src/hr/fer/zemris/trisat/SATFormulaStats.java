package hr.fer.zemris.trisat;



public class SATFormulaStats {
	
	private SATFormula formula;
	private boolean isSatisfied;
	private int numberOfSatisfied;
	private double percentageBonus;
	private double[] post;
    private final double PERCENTAGE_UP = 0.01;
    private final double PERCENTAGE_DOWN = 0.1;
    private final int percentageUnitAmount = 50;
	
	public SATFormulaStats(SATFormula formula) {
		this.formula = formula;
		
		this.post = new double[formula.getNumberOfClauses()];
	}
	
	// analizira se predano rješenje i pamte svi relevantni pokazatelji
	public void setAssignment(BitVector assignment, boolean updatePercentages) {
		this.isSatisfied = formula.isSatisfied(assignment);
		this.numberOfSatisfied = 0;
        for (int i = 0; i < formula.getNumberOfClauses(); ++i) {
            Clause c = formula.getClause(i);
            double updatePost = 0;

            if (c.isSatisfied(assignment)) {
                updatePost = (1 - post[i]) * PERCENTAGE_UP;
                numberOfSatisfied++;
                percentageBonus += percentageUnitAmount * (1 - post[i]);

            } else {
                updatePost = (0 - post[i]) * PERCENTAGE_DOWN;
                percentageBonus -= percentageUnitAmount * (1 - post[i]);
            }

            if (updatePercentages) {
                post[i] += updatePost;
            }
}
	}
	
	// vraća temeljem onoga što je setAssignment zapamtio: broj klauzula koje su zadovoljene
	public int getNumberOfSatisfied() {
		return this.numberOfSatisfied;
	}
	
	// vraća temeljem onoga što je setAssignment zapamtio
	
	public boolean isSatisfied() {
		return this.isSatisfied;
	}
	
	// vraća temeljem onoga što je setAssignment zapamtio: suma korekcija klauzula
	public double getPercentageBonus() {
		return percentageBonus;
	}
	// vraća temeljem onoga što je setAssignment zapamti :procjena postotka za klauzulu
	public double getPercentage(int index) {
		return post[index];
	}
}
