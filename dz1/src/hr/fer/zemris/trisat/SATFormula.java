package hr.fer.zemris.trisat;


import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SATFormula {
	
	private int numberOfVariables;
	private Clause[] clauses;
	public SATFormula(int numberOfVariables, Clause[] clauses) {
		this.numberOfVariables = numberOfVariables;
		this.clauses = Arrays.copyOf(clauses, clauses.length);
	}
	
	public int getNumberOfVariables() {
		return numberOfVariables;
	}
	
	public int getNumberOfClauses() {
		return this.clauses.length;
	}
	
	public Clause getClause(int index) {
		 return this.clauses[index];
	}
	
	public boolean isSatisfied(BitVector assignment) {
		for (Clause clause : clauses) {
			if (!clause.isSatisfied(assignment)) {
				return false;
			}
		}
		return true;
	}
	
	@Override
	public String toString() {
		String returnString = "";
		for (Clause clause : clauses) {
			
			returnString += clause.toString() + "\n";
			
		}
		return returnString;
	}
	public List<Clause> nezadovoljeneKlauzule(BitVector assignment) {
		List<Clause> klauzule = new LinkedList<>();
		for (Clause clause : clauses) {
			if (!clause.isSatisfied(assignment)) {
				klauzule.add(clause);
			}
		}
		return klauzule;
	}
}
