package hr.fer.zemris.trisat;

public class MutableBitVector extends BitVector {
	
	public MutableBitVector(boolean... bits) {
		super(bits);
	}
	public MutableBitVector(int n) {
		super(n);
	}
	
	// zapisuje predanu vrijednost u zadanu varijablu
	public void set(int index, boolean value) {
		if (index < 0 || index > elements.length) throw new IllegalArgumentException();
		this.elements[index] = value;
	}
	
}
