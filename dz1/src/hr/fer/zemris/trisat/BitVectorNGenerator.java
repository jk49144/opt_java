package hr.fer.zemris.trisat;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class BitVectorNGenerator implements Iterable<MutableBitVector> {
	//vektor cije susjedstvo racunamo
	private BitVector assignment;
	
	public BitVectorNGenerator(BitVector assignment) {
		this.assignment = assignment;
	}
	
	// Vraća iterator koji na svaki next() računa sljedećeg susjeda
	@Override
	public Iterator<MutableBitVector> iterator() {
		return new Iterator<MutableBitVector>() {
			int i = 0;
			@Override
			public boolean hasNext() {
				return i < assignment.getSize();
			}

			@Override
			public MutableBitVector next() {
				if(!hasNext()) {
					throw new IndexOutOfBoundsException();
				}
				MutableBitVector newVector = assignment.copy();
				newVector.set(i, !newVector.get(i));
				i++;
				return newVector;
			}
			
		};
		
		
	}
	
	//vraca se cijelo polje susjeda.... susjed prema hammingovu kodu
	//odnosno svaki susjedni vektor ima samo jedan bit drugaciji od 
	//susjeda.
    public MutableBitVector[] createNeighborhood() {
        List<MutableBitVector> neighborhood = new ArrayList<>();
        iterator().forEachRemaining(t -> neighborhood.add(t));

        MutableBitVector[] array = new MutableBitVector[neighborhood.size()];
        return neighborhood.toArray(array);
}
	
}
