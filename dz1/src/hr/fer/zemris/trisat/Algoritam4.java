package hr.fer.zemris.trisat;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Algoritam4 {
	private final int MAX_NUMBER_OF_ITERATIONS = 100000;
	private int  maxTries = 10;
	
	public void solve(SATFormula formula) {
		for (int x = 0; x < maxTries; x++) {
			BitVector initial = new BitVector(new Random(), formula.getNumberOfVariables());
			SolutionCandidate solutionCandidate = new SolutionCandidate(initial, 0.0);
			int i = 0;
			SATFormulaStats stats = new SATFormulaStats(formula);
		
			while(i < MAX_NUMBER_OF_ITERATIONS) {
			
				stats.setAssignment(solutionCandidate.getBitVector(), true);
				solutionCandidate.setFitness(getFitness(stats));
				if(formula.getNumberOfClauses() == stats.getNumberOfSatisfied()) {
					System.out.println("Rjesenje je: " + solutionCandidate.getBitVector().toString() + 
							" nadeno" + i +"-tom koraku " + x + "te iteracije");
					return;
				}
				List<SolutionCandidate> neighbourhoodCandidates = createNeigbourhood(solutionCandidate, stats);
				SolutionCandidate newSolution = nextCandidate(neighbourhoodCandidates);
				if (newSolution.getFitness() < solutionCandidate.getFitness()) {
					System.out.println("Zapeli smo u lokalnom maksimumu koji iznos" + solutionCandidate.getBitVector().toString()+ "nakon kolkko iteracija" 
							+ i + "fitnes iznosi" +newSolution.getFitness());
					break;
				}
				solutionCandidate = newSolution;
				i++;
			}
			//System.out.println("Dosli do maksimalnog broja iteracija zadnji broj koji smo imali je:  " + solutionCandidate.getBitVector().toString()
					//+ "ciji je fitness" + solutionCandidate.getFitness());
		}
	}
	
	
	private double getFitness(SATFormulaStats stats) {
		return stats.getNumberOfSatisfied();
	}
	
	
	private List<SolutionCandidate> createNeigbourhood(SolutionCandidate solutionCandidate, SATFormulaStats stats) {
		BitVectorNGenerator generator = new BitVectorNGenerator(solutionCandidate.getBitVector());
		List<SolutionCandidate> neighbourhoodCandidates = new ArrayList<>();
		for (BitVector candidate : generator) {
			stats.setAssignment(candidate, false);
			neighbourhoodCandidates.add(new SolutionCandidate(candidate, getFitness(stats)));
		}
		return neighbourhoodCandidates;
	}
	
	
	private SolutionCandidate nextCandidate(List<SolutionCandidate> neighbourhoodCandidates) {
		double maxFitness = 0.0;
		List<SolutionCandidate> bestCandidates = new ArrayList<>();
		for(SolutionCandidate sCandidate : neighbourhoodCandidates) {
			if (sCandidate.getFitness() >= maxFitness) {
				maxFitness = sCandidate.getFitness();
				
			}
		}
		for(SolutionCandidate sCandidate : neighbourhoodCandidates) {
			if (sCandidate.getFitness() == maxFitness) {
				bestCandidates.add(sCandidate);
				
			}
		}
		
		return bestCandidates.get(new Random().nextInt(bestCandidates.size()));
	}
}
