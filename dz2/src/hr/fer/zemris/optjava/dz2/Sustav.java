package hr.fer.zemris.optjava.dz2;

import java.util.Random;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealVector;
/**
 * 23.10.2017
 * @author Jurij Kos
 * Starting point for the program that finds solution of n equations with n unknowns
 * solving like a optimization problem.
 */
public class Sustav {
	/**
	 * Method generates random starting point with all values in range [-5, 5]
	 * @param args [0]grad or newton for choosing between gradientDescent method and newton method
	 * 				[1] maxNumberOfIterations
	 * 				[2] path to file
	 */
	public static void main(String[] args) {
		if(args.length != 3) {
			throw new IllegalArgumentException();
		}
		String method = args[0];
		int maxNumberOfIterations = Integer.parseInt(args[1]);
		String path = args[2];//"sustav.txt";
		LinearSystemFunction function = new LinearSystemFunction(path);
		double[] values = new double[function.vectorDimension()];
		Random radnom = new Random();
		for (int i = 0; i < function.vectorDimension(); i++) {
			values[i] = radnom.nextDouble() * 10.0 - 5.0;
		}
		
		RealVector point = MatrixUtils.createRealVector(values); 
		if (method.equals("grad")) {
			NumOptAlgorithm.gradientDescentAlgorithm(point, function, maxNumberOfIterations);
			System.out.println("suloution");
			System.out.println(function.solveClassic());
			//RealVector v = function.solveClassic();
			//NumOptAlgorithm.gradientDescentAlgorithm(v,function,maxNumberOfIterations);
		}
		else if (method.equals("newton")) {
			NumOptAlgorithm.newtonAlgorithm(point, function, maxNumberOfIterations);
		}
	}
}
