package hr.fer.zemris.optjava.dz2;

import java.util.Random;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealVector;
/**
 * Starting point of program
 * We have function and file with some points and value in those points.
 * Our program searches for coefficients of ourfunction.
 * @author jurij
 *
 */
public class Prijenosna {
	public static void main(String[] args) {
		if(args.length != 3) {
			throw new IllegalArgumentException();
		}
		
		String method = args[0];
		int maxNumberOfIterations = Integer.parseInt(args[1]);
		String path = "prijenosna.txt";//args[2];
		CoeficientFinder function = new CoeficientFinder(path);
		double[] values = new double[function.vectorDimension()];
		Random radnom = new Random();
		for (int i = 0; i < function.vectorDimension(); i++) {
			values[i] = radnom.nextDouble() * 10.0 - 5.0;
		}
		
		RealVector point = MatrixUtils.createRealVector(values); 
		if (method.equals("grad")) {
			NumOptAlgorithm.gradientDescentAlgorithm(point, function, maxNumberOfIterations);
			
		}
		else if (method.equals("newton")) {
			NumOptAlgorithm.newtonAlgorithm(point, function, maxNumberOfIterations);
		}
		System.out.println(function.evaluateByCoefficients(new double[] {7, -3, 2, 1, -3, 3}));
	}

}
