package hr.fer.zemris.optjava.dz2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
/**
 * 23.10.2017
 * @author Jurij Kos
 *Class LinearSystemFunction represents linear system of n equations with n unknowns.
 *Linear system is represented with matrixes Ax = b 
 *We are trying to find x with these program.
 */
public class LinearSystemFunction implements IHFunction {
	/**
	 * coefficients of our linear system
	 * 
	 */
	private RealMatrix AMatrix;
	/**
	 * y  values of every equation
	 */
	private RealMatrix bMatrix;
	/**
	 * Parse file to find matrixes A and b
	 * @param path path to file 
	 */
	public LinearSystemFunction(String path) {
		try {
			Scanner scanner = new Scanner(new File(path));
			List<String> list = new ArrayList<>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("#")) {
					continue;
				}
				if (line.startsWith("[")) {
					line = line.substring(1);
				}
				if (line.endsWith("]")) {
					line =line.substring(0, line.length() - 1);
				}
				list.add(line);
			}
			double [][] A = new double[list.size()][list.get(0).split("\\s+").length - 1];
			double [] b = new double[list.size()]; 
			int j = 0;
			for (String string : list) {
				String[] poljeStringova = string.split(",\\s+");
				for (int i = 0; i < poljeStringova.length; i++) {
					if (i == poljeStringova.length - 1) {
						b[j] = Double.parseDouble(poljeStringova[i]);
					}
					else {
						A[j][i] = Double.parseDouble(poljeStringova[i]);
					}
				}
				j++;
			}
			AMatrix = MatrixUtils.createRealMatrix(A);
			bMatrix = MatrixUtils.createColumnRealMatrix(b);
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	@Override
	public int vectorDimension() {
		return AMatrix.getColumnDimension();
	}

	@Override
	public double evaluateFunction(RealVector point) {
		if (point.getDimension() != vectorDimension()) {
			throw new IllegalArgumentException();
		}
		
		RealMatrix matrix = AMatrix.multiply(MatrixUtils.createColumnRealMatrix(point.toArray()));
		RealVector solution = matrix.add(bMatrix.scalarMultiply(-1.0)).getColumnVector(0);
		
		return solution.getNorm();
	}
	// 2 A transp (Ax - b)
	@Override
	public RealVector getGradient(RealVector point) {


        RealMatrix x = MatrixUtils.createColumnRealMatrix(point.toArray());

        RealMatrix tmp = AMatrix.multiply(x).subtract(bMatrix);
        RealMatrix sol = AMatrix.transpose().scalarMultiply(2).multiply(tmp);
        return sol.getColumnVector(0);
	}
	//2A trasp A
	@Override
	public RealMatrix hesseMatrix(RealVector point) {
		if (point.getDimension() != vectorDimension()) {
			throw new IllegalArgumentException();
		}
		return AMatrix.transpose().scalarMultiply(2).multiply(AMatrix);
	}
	/**
	 * Solves problem in simple classic way{A inverse * b = x}. Used to check how close solution we got using
	 * our algorithms is.
	 * @return solution of system
	 */
	public RealVector solveClassic() {
		return MatrixUtils.inverse(AMatrix).multiply(bMatrix).getColumnVector(0);
				
	}

}
