package hr.fer.zemris.optjava.dz2;



import org.apache.commons.math3.linear.RealVector;

/**
 * 22.10.2017
 * @author Jurij Kos
 * Represents scalar function over n-dimensional
 * vector of real numbers.
 */
public interface IFunction {
	/**
	 * 
	 * @return dimension of vector
	 */
	public int vectorDimension();
	
	/**
	 * 
	 * @return value of function for point
	 */
	public double evaluateFunction(RealVector point);
	/**
	 * 
	 * @return gradient of our function in current point.
	 */
	public RealVector getGradient(RealVector point);

}
