package hr.fer.zemris.optjava.dz2;

import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
/**
 * 22.10.2017
 * @author Jurij Kos
 *Interface IHFunction extends interface IFunction with method hesseMatrix 
 */
public interface IHFunction extends IFunction {

	/**
	 * It describes the local curvature of a function of many variables. The 
	 * Hessian matrix was developed in the 19th century by the German mathematician
	 * Ludwig Otto Hesse and later named after him.
	 * @param point
	 * @return hesseMatrix
	 */
	public RealMatrix hesseMatrix(RealVector point);
}
