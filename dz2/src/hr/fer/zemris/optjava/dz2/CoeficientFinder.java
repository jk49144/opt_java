package hr.fer.zemris.optjava.dz2;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class CoeficientFinder implements IHFunction {
	private double[][] xValues;
	private double[] yValues;

	/**
	 * Parse file from location path
	 * 
	 * @param path
	 */
	public CoeficientFinder(String path) {
		try {
			Scanner scanner = new Scanner(new File(path));
			List<String> list = new ArrayList<>();
			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				if (line.startsWith("#")) {
					continue;
				}
				if (line.startsWith("[")) {
					line = line.substring(1);
				}
				if (line.endsWith("]")) {
					line = line.substring(0, line.length() - 1);
				}
				list.add(line);
			}
			double[][] A = new double[list.size()][list.get(0).split("\\s+").length - 1];
			double[] b = new double[list.size()];
			int j = 0;
			for (String string : list) {
				String[] poljeStringova = string.split(",\\s+");
				for (int i = 0; i < poljeStringova.length; i++) {
					if (i == poljeStringova.length - 1) {
						b[j] = Double.parseDouble(poljeStringova[i]);
					} else {
						A[j][i] = Double.parseDouble(poljeStringova[i]);
					}
				}
				j++;
			}
			xValues = A;
			yValues = b;
			scanner.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public int vectorDimension() {
		return 6;
	}

	@Override
	public double evaluateFunction(RealVector point) {
		if (point.getDimension() != vectorDimension()) {
			throw new IllegalArgumentException();
		}
		double error = 0.0;
		for (int i = 0; i < xValues.length; i++) {
			error += Math.pow(evaluateRowAt(i, point.toArray()), 2);
		}
		error = Math.pow(error, 0.5);

		return error;
	}

	private double evaluateRowAt(int i, double[] values) {
		double[] x = xValues[i];
		double error = values[0] * x[0] + values[1] * Math.pow(x[0], 3) * x[1]
				+ values[2] * Math.exp(values[3] * x[2]) * (1 + Math.cos(values[4] * x[3]))
				+ values[5] * x[3] * Math.pow(x[4], 2);
		error = error - yValues[i];
		return error;
	}

	@Override
	public RealVector getGradient(RealVector point) {
		double[] solution = point.toArray();
		double[] gradient = new double[vectorDimension()];
		for (int i = 0; i < xValues.length; i++) {
			double[] x = xValues[i];
			double val = evaluateRowAt(i, solution);
			double expFactor = Math.exp(solution[3] * x[2]);
			double cosfactor = (1 + Math.cos(solution[4] * x[3]));
			gradient[0] += val * x[0];
			gradient[1] += val * Math.pow(x[0], 3) * x[1];
			gradient[2] += val * expFactor * cosfactor;
			gradient[3] += val * solution[2] * expFactor * cosfactor * x[2];
			gradient[4] += val * solution[2] * expFactor * (-1) * Math.sin(solution[4] * x[3]) * x[3];
			gradient[5] += val * x[3] * Math.pow(x[4], 2);

		}
		return MatrixUtils.createRealVector(gradient).unitVector();
	}

	@Override
	public RealMatrix hesseMatrix(RealVector point) {

		return null;

	}

	/**
	 * Returns how far our solution is in L2 norm from good solution.
	 * 
	 */
	public double evaluateByCoefficients(double[] values) {
		double[] x = xValues[1];
		double val = values[0] * x[0] + values[1] * Math.pow(x[0], 3) * x[1]
				+ values[2] * Math.exp(values[3] * x[2]) * (1 + Math.cos(values[4] * x[3]))
				+ values[5] * x[3] * Math.pow(x[4], 2);
		return val - yValues[1];
	}

}
