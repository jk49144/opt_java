package hr.fer.zemris.optjava.dz2;

import java.util.Random;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealVector;
/**
 * 23.10.2017
 * This class is staring point for solving 2 eqaustions with
 * method of gradient descent and newton method
 * @author Jurij Kos
 *
 */
public class Jednostavno {
	/**
	 * Main takes 2 arguments from command line
	 * 
	 * @param args[0] 1a , 1b, 2a, 2b first letter is for choosing beetwen function1 and 2 
	 * 				a means we want to use gradient descent algorithm for finding solution if b we choose newton method
	 * 		  args[1] maximum number of iterations 
	 * 
	 */
	public static void main(String[] args) {
		String task;
		int maxNumberOfIterations;
		double startingPointX;
		double startingPointY;
		if(args.length == 2) {
			task = args[0];
			maxNumberOfIterations = Integer.parseInt(args[1]);
			startingPointX = new Random().nextDouble() * 10 - 5;
			startingPointY = new Random().nextDouble() * 10 - 5;
		}
		else if (args.length == 4) {
			task = args[0];
			maxNumberOfIterations = Integer.parseInt(args[1]);			
			startingPointX = Double.parseDouble(args[2]);
			startingPointY = Double.parseDouble(args[3]);
		}
		else {
			throw new IllegalArgumentException();
		}
		RealVector point = MatrixUtils.createRealVector(new double[] {startingPointX, startingPointY});
		if(task.equals("1a")) {
			IFunction function = new Function1();
			NumOptAlgorithm.gradientDescentAlgorithm(point, function, maxNumberOfIterations);
		}
		else if (task.equals("1b")) {
			IFunction function = new Function1();
			NumOptAlgorithm.gradientDescentAlgorithm(point, function, maxNumberOfIterations);
		}
		else if (task.equals("2a")) {
			IHFunction function = new Function2();
			NumOptAlgorithm.gradientDescentAlgorithm(point, function, maxNumberOfIterations);
			
		}
		else if (task.equals("2b")) {
			IHFunction function = new Function2();
			NumOptAlgorithm.newtonAlgorithm(point, function, maxNumberOfIterations);
		}
		else {
			System.out.println("Wrong first argument");
		}
	}
}
