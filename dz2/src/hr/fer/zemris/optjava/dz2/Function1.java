package hr.fer.zemris.optjava.dz2;

import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;
/**
 * 22.10.2017
 * @author Jurij Kos
 *Function 1 implements function f(x1,x2) = x1^2 +(x2 -1)^2
 */
public class Function1 implements IHFunction {

	
	@Override
	public int vectorDimension() {
		return 2;
	}

	@Override
	public double evaluateFunction(RealVector point) {
		if(point.getDimension() != 2) {
			throw new IllegalArgumentException();
		}
		return Math.pow(point.getEntry(0), 2) + Math.pow(point.getEntry(1) - 1, 2);
	}

	@Override
	public RealVector getGradient(RealVector point) {
		if(point.getDimension() != 2) {
			throw new IllegalArgumentException();
		}
		return MatrixUtils.createRealVector(new double[] {2 * point.getEntry(0), 2 * (point.getEntry(1) - 1)});
	}

	@Override
	public RealMatrix hesseMatrix(RealVector point) {
		if (point.getDimension() != 2) {
			throw new IllegalArgumentException();
		}
		return MatrixUtils.createRealMatrix(new double[][]{{2, 0}, {0, 20}});
	}

}
