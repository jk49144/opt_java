package hr.fer.zemris.optjava.dz2;


import java.util.Arrays;

import org.apache.commons.math3.linear.LUDecomposition;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;
import org.apache.commons.math3.linear.RealVector;

public class NumOptAlgorithm {
	public static final double EPSILON = 0.001;;
	public static RealVector gradientDescentAlgorithm(RealVector point, IFunction function, int maxNumberOfIterations) {
		RealVector x = point.copy();
		System.out.println(x + "; f = " + function.evaluateFunction(x));
		for (int i = 0; i < maxNumberOfIterations; i++) {
			if (nearOptimum(x, function)) {
				break;
			}
			RealVector direction = getDirection(x, function);
			double lambdaOptimum = bisectionAlgorithm(x, direction, function);
			x = x.add(direction.mapMultiply(lambdaOptimum));
			System.out.println(x + "; f = " + function.evaluateFunction(x));
			
			
		}
		return x;
	}
	public static RealVector newtonAlgorithm(RealVector point, IHFunction function, int maxNumberOfIterations) {
		RealVector x = point.copy();
		System.out.println(x + "; f = " + function.evaluateFunction(x));
		for (int i = 0; i < maxNumberOfIterations; i++) {
			if (nearOptimum(x, function)) {
				break;
			}
			RealVector direction = getNewtonDirection(x, function);
			double lambdaOptimum = bisectionAlgorithm(x, direction, function);
			x = x.add(direction.mapMultiply(lambdaOptimum));
			System.out.println(x + "; f = " + function.evaluateFunction(x));
		}
		return x;
	}
	
	private static RealVector getDirection(RealVector point, IFunction function) {
		return function.getGradient(point).unitVector().mapMultiply(-1.0);
		
	}
	
	private static RealVector getNewtonDirection (RealVector point, IFunction function) {
        RealMatrix gradient = MatrixUtils.createColumnRealMatrix(function.getGradient(point).toArray());
        RealMatrix hesse = ((IHFunction) function).hesseMatrix(point);
        RealMatrix invHesse = new LUDecomposition(hesse).getSolver().getInverse();

        RealMatrix sol = invHesse.multiply(gradient);
        RealVector solVector = sol.getColumnVector(0);
        return solVector.mapMultiply(-1).unitVector();
	}
	
	private static boolean nearOptimum(RealVector point, IFunction function) {
		double[] value = function.getGradient(point).toArray();
		return Arrays.stream(value).allMatch(val -> Math.abs(val) < EPSILON);
	}
	private static double bisectionAlgorithm(RealVector point, RealVector direction, IFunction function) {
        RealVector x = point.copy();
        double lo = 0;
        double hi = findLambdaUpper(function, point, direction);
        int i = 100;
        while (i > 0) {
            i--;
        	double mid = (lo + hi) / 2;
            x = point.add(direction.mapMultiply(mid));

            double der = function.getGradient(x).dotProduct(direction);
            if (Math.abs(der) < EPSILON) {
                return mid;
            }

            if (der < 0) {
                lo = mid;
            } else {
                hi = mid;
            }
        }
        return lo;
		
	}
	private static double findLambdaUpper(IFunction function, RealVector point, RealVector direction) {
        double lambdaUpper = 1;
        RealVector x = point.copy();

        while (function.getGradient(x).dotProduct(direction) < 0) {
            lambdaUpper *= 2;
            x = point.add(direction.mapMultiply(lambdaUpper));
        }
        return lambdaUpper;
	}

}
